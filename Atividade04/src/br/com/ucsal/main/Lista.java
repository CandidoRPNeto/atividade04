package br.com.ucsal.main;

public class Lista {

	private Object[] objeto;
	private int lastPosi = 0;

	public Lista(Object[] objeto) {
		this.objeto = objeto;
	}

	public void incluir(Object objeto) {
		this.objeto[lastPosi] = objeto;
		lastPosi++;
	}

	public void remover(int posicao) {
		for (int i = posicao; i < lastPosi; i++) {
			if (i < lastPosi - 1)
				objeto[i] = objeto[i + 1];
		}
		objeto[lastPosi] = null;
		lastPosi--;
	}

	public Object consultar(int posicao) {
		return objeto[posicao];
	}

	public Integer contagem() {
		return lastPosi;
	}

	public void limpar() {
		for (int i = 0; i < lastPosi; i++) {
			objeto[i] = null;
		}
	}

	public void listar() {
		for (int i = 0; i < objeto.length; i++) {
			System.out.println(objeto[i] + "\n\n");
		}
	}

}
