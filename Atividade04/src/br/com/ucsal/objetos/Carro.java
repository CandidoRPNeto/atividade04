package br.com.ucsal.objetos;

import br.com.ucsal.entidades.Pessoa;

public class Carro {
	private String placa, nome;
	private Integer anoDeFabricacao, valor;
	private Pessoa proprietario;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Integer getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public void setAnoDeFabricacao(Integer anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public Pessoa getProprietario() {
		return proprietario;
	}

	public void setProprietario(Pessoa proprietario) {
		this.proprietario = proprietario;
	}

	public String toString() {
		return "Nome: " + nome + "\nPlaca: " + placa + "\nValor: " + valor + "\nAno de Fabricao: " + anoDeFabricacao
				+ "\nProprietario" + proprietario;
	}
}
