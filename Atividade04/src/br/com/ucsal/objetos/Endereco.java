package br.com.ucsal.objetos;

public class Endereco {
	private Integer cep, numero;
	private String rua, bairro, logradouro;

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public Integer getCep() {
		return cep;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public Integer getNumero() {
		return numero;
	}

	public String getRua() {
		return rua;
	}

	public String toString() {
		return "Rua: " + rua + "\nBairro: " + bairro + "\nCEP: " + cep + "\nN�mero: " + numero
				+ "\nLogradouro" + logradouro;
	}
}
