package br.com.ucsal.teste;

import br.com.ucsal.entidades.Pessoa;
import br.com.ucsal.objetos.Carro;
import br.com.ucsal.objetos.Endereco;

public class TestaGeral {
	private static int lastPosi = 0, tam = 0;

	private static Pessoa[] pessoas;

	public static void main(String[] args) {
		iniciarPrograma();
	}

	public static void iniciarPrograma() {
		criarLista(10);

		Endereco casaDoJoao = new Endereco();
		Pessoa joao = new Pessoa();
		Carro honda = new Carro();

		criarEndereco(casaDoJoao, 13415435, 234, "Av Santos Drimond", "Rua Augostinho Carrara", "Residencia");
		criarCarro(honda, 15000, "Honda civic", "DFSA023", 2010);
		criarPessoa(joao, 3324134, "Jo�o de Melo Dantas", "(71) 9974-8765", casaDoJoao);
	}

	private static void criarLista(int tamanho) {
		tam = tamanho;
		pessoas = new Pessoa[tam];
	}

	public static void criarEndereco(Endereco endereco, Integer cep, Integer numero, String bairro, String rua,
			String logradouro) {
		endereco.setBairro(bairro);
		endereco.setRua(rua);
		endereco.setLogradouro(logradouro);
		endereco.setCep(cep);
		endereco.setNumero(numero);
	}

	public static void criarCarro(Carro carro, Integer valor, String nome, String placa, Integer anoDeFabricacao) {
		carro.setNome(nome);
		carro.setPlaca(placa);
		carro.setValor(valor);
		carro.setAnoDeFabricacao(anoDeFabricacao);
	}

	public static void criarPessoa(Pessoa pessoa, Integer cpf, String nome, String telefone, Endereco endereco) {
		pessoa.setCpf(cpf);
		pessoa.setEndereco(endereco);
		pessoa.setNome(nome);
		pessoa.setTelefone(telefone);
		pessoas[lastPosi] = pessoa;
		lastPosi++;
	}

	public static void listarPessoas() {
		if (lastPosi == 0)
			System.out.println("Sem pessoas");
		else {
			for (int i = 0; i < lastPosi; i++) {
				System.out.println(pessoas[i] + "\n\n\n");
			}
		}
	}

}
